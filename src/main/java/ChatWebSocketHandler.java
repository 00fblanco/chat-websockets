import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class ChatWebSocketHandler {
	private String sender, message;
	
	@OnWebSocketConnect
	public void onConnect(Session user) throws Exception{
		String username = "User" + Application.nextUserNumber++;
		Application.userUsernameMap.put(user, username);
		Application.broadcastMessage(sender ="Server", message = (username + " joined the chat"));
	}
	
	@OnWebSocketClose
	public void onClose(Session user, int statusCode, String reason) {
		String username = Application.userUsernameMap.get(user);
		Application.userUsernameMap.remove(user);
		Application.broadcastMessage(sender ="Server", message = (username + " left the chat"));
	}
	
	@OnWebSocketMessage
	public void onMessage(Session user, String message) {
		Application.broadcastMessage(sender = Application.userUsernameMap.get(user), message = message);
	}
	
}
