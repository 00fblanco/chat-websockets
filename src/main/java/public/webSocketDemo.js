var webSocket = new WebSocket(`ws://${location.hostname}:${location.port}/chat`);

webSocket.onmessage = msg => updateChat(msg);

webSocket.onclose = () => alert('WebSocket connection closed');

id("send").addEventListener("click", () => {
	sendMessage(id("message").value);
});

id("message").addEventListener("keypress", (e) => {
	if(e.keyCode === 13) sendMessage(e.target.value);
})

function sendMessage(message){
	if(message !== ""){
		webSocket.send(message);
		id("message").value = "";
	}
}

function updateChat(message){
	var data = JSON.parse(message.data);
	insert("chat", data.userMessage);
	id("userlist").innerHTML = "";
	data.userList.forEach( user => {
		insert("userlist", `<li>${user}</li>`);
	});
}

function insert(targetId, message){
	id(targetId).insertAdjacentHTML("afterbegin", message);
}

function id(id){
	return document.getElementById(id);
}



